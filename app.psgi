#!/usr/bin/env perl

use strict;
use warnings;

use Carp qw/confess/;

use Plack::Request;
use CHI;
use Furl;
use JSON::XS;
use URI::Amazon::APA;

use File::Spec;
use File::Basename;

sub env_value {
  my $key = shift;
  my $env_key = "AMAZON_PROXY_${key}";

  if ( defined( my $val = $ENV{$env_key} ) ) {
    return $val;
  }

  confess("environment value of ${env_key} is not defined.");
}

my $distdir   = File::Spec->rel2abs( dirname(__FILE__) );
my $cachedir  = File::Spec->catdir( $distdir, '.cache' );

my $api_key     = env_value('API_KEY');
my $api_secret  = env_value('API_SECRET');
my $api_id      = env_value('API_ID');

my $agent = Furl->new(
  agent   => 'AmazonProxy/0.01',
  timeout => 10,
);

my $cache = CHI->new(
  driver    => 'File',
  root_dir  => $cachedir,
);

my $app   = sub {
  my $req   = Plack::Request->new(shift);

  my $asin  = $req->query_parameters->get('asin')
    or return $req->new_response(400)->finalize;

  if ( defined(my $message = $cache->get($asin)) ) {
    return $req->new_response(
      200,
      [
        'Content-Type'    => 'application/json; charset=utf-8',
        'Content-Length'  => length($message),
      ],
      [ $message ]
    )->finalize;
  }

  my $uri   = URI::Amazon::APA->new('http://webservices.amazon.co.jp/onca/xml');
  $uri->query_form(
    Services      => 'AWSECommerceService',
    Operation     => 'ItemLookup',
    ResponseGroup => 'Medium',
    IdType        => 'ASIN',
    ItemId        => $asin,
    AssociateTag  => $api_id,
  );
  $uri->sign(
    key     => $api_key,
    secret  => $api_secret,
  );

  my $res = $agent->get($uri);

  if ( ! $res->is_success ) {
    my $message = "API Call Error: " . $res->status_line;

    return $req->new_response(
      503,
      [
        'Content-Type'    => 'text/plain; charset=utf-8',
        'Content-Length'  => length($message),
      ],
      [ $message ],
    )->finalize;
  }

  my $content = $res->content;
  my ( $item_url )  = ( $content =~ m{<DetailPageURL>\s*(.+?)\s*</DetailPageURL>} );
  my ( $image_url ) = ( $content =~ m{<MediumImage>\s*<URL>\s*(.+?)\s*</URL>} );

  $image_url =~ s{_SL\d+_}{_SL160_};

  my $message = encode_json +{
    permalink => $item_url,
    image     => $image_url,
  };

  $cache->set($asin, $message, '24h');

  return $req->new_response(
    200,
    [
      'Content-Type'    => 'application/json; charset=utf-8',
      'Content-Length'  => length($message),
    ],
    [
      $message
    ]
  )->finalize;
};
